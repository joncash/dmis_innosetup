; DMIS Release Setup
; ========================  variables define ==============================

#define MyAppName "DMIS"
;目前版本---------------------------------------改這裡
#define MyAppVersion "0.0.0.21"

;廠商版本 1.1.0.63---------------------------------------改這裡
#define oldVersion "0.0.0.20"

#define MyAppPublisher "Joncash Studio"
#define MyAppExeName "DMIS.exe"
#define MyIco "DMIS_Setup128.ico"

; ************* 來源檔案路徑, 注意是 Release or Debug---------------------------------------改這裡
#define ReleaseDir "D:\01_Repos\ExtRepo\DMIS\DMIS\bin\Debug"
;#define ReleaseDir "D:\01_Repos\ExtRepo\DMIS\DMIS\bin\Release"



;版權宣告檔案
#define LicenseFile "D:\AOM_Installer\DMIS\license.rtf"

;輸出目錄及檔名---------------------------------------改這裡
#define OutDir "D:\Publish"
#define OutFile "DMIS_Setup_"

;Setup Icon File
#define SetupIconFile "D:\AOM_Installer\DMIS\images\DMIS_Setup128.ico"

;Program Icon File
#define MyAppIcon "D:\AOM_Installer\DMIS\images\DMIS_Setup128.ico"

#define ReleaseInfo "D:\AOM_Installer\DMIS\release0.0.0.4.txt"

;************ Exclude Files ;是否忽略 .config 檔案, 要注意
#define ExcludeFiles "*.pdb,*.vshost.*,*.config"

;========== Git ==================
#define gitPath "C:\Program Files (x86)\Git\bin"
#define gitIgnore "D:\AOM_Installer\.gitignore"
#define backupBatch "D:\backup_Hanbo.bat"
#define restoreBatch "D:\restore_Hanbo.bat"
#define workingDrive "D:"
#define restoreLog "D:\AOM_Installer\restore.txt"
#define installDir "D:\HanboApps"


[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)

; We use Project GUID
AppId={{556F9629-793F-4404-AF46-97DB5E841B22}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
LicenseFile={#LicenseFile}
InfoAfterFile={#ReleaseInfo}
OutputDir={#OutDir}
OutputBaseFilename={#OutFile}{#MyAppVersion}
SetupIconFile={#SetupIconFile}
Compression=lzma
SolidCompression=yes


[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "Install";      Description: 安裝 {#MyAppVersion};  Flags: exclusive

Name: "desktopicon";  Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
;Git ignoreFile

;安裝檔案
Source: "{#ReleaseDir}\DMIS.exe"; DestDir: "{app}"; Flags: ignoreversion ; Tasks: Install ;
Source: "{#ReleaseDir}\*"; Excludes:"{#ExcludeFiles}"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs ;Tasks: Install
;Source: "{#PluginDll}"; DestDir: "{app}"; Flags: ignoreversion ; Tasks: Install
Source: "{#MyAppIcon}"; DestDir: "{app}"; Flags: ignoreversion ; Tasks: Install
;Source: "{#ReleaseDir}\Documents\AOM使用手冊.pdf"; DestDir: "{app}"; Flags: isreadme ;Tasks: Install



; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; IconFilename: "{app}\{#MyIco}";
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon
Name: "{userdesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}";WorkingDir: "{app}"; IconFilename: "{app}\{#MyIco}"; Comment: "Shortcut"
;Document
;Name: "{group}\{#MyAppName} 使用手冊"; Filename: "{app}\Documents\AOM使用手冊.pdf"; WorkingDir: "{app}\Documents"

[Code]
procedure Backup();
var 
  OutFile:  String;
  ResultCode: Integer;
begin
  OutFile:= ExpandConstant('{#backupBatch}');
  //delete first
  DeleteFile(OutFile);

  //build batch file
  SaveStringToFile(OutFile,'REM *** define variables ***' + #13#10, True);
  SaveStringToFile(OutFile,'SET gitPath={#gitPath}' + #13#10, True);
 
  SaveStringToFile(OutFile,'SET version=' + ExpandConstant('{#oldVersion}') + #13#10, True);
  SaveStringToFile(OutFile,'SET workingDrive=' + ExpandConstant('{#workingDrive}') + #13#10, True);
  SaveStringToFile(OutFile,'SET appDir=' + ExpandConstant('{app}') + #13#10, True);
  SaveStringToFile(OutFile,'SET backupTime=%date:~0,10% %time%' + #13#10, True);  
  SaveStringToFile(OutFile,'SET backupMsg="backup, %backupTime%"' + #13#10, True);  
  
  SaveStringToFile(OutFile,'SET tagMsg="backup version %version%"' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** setting the git.exe into system path ***' + #13#10, True);
  SaveStringToFile(OutFile,'PATH %gitPath%;%PATH%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** change the directory to AOM directory ***' + #13#10, True);
  SaveStringToFile(OutFile,'%workingDrive%' + #13#10, True);
  SaveStringToFile(OutFile,'cd %appDir%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** add and commit current version ***' + #13#10, True);
  SaveStringToFile(OutFile,'git add --all' + #13#10, True);
  SaveStringToFile(OutFile,'git commit -m %backupMsg%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** Tag it, in order to restore files easily ***' + #13#10, True);
  SaveStringToFile(OutFile,'git tag -a %version% -m %tagMsg%' + #13#10, True);

  //Execute batch file
 if Exec(OutFile, '', '', SW_SHOW,
     ewWaitUntilTerminated, ResultCode) then
  begin
    log('Success');
    // handle success if necessary; ResultCode contains the exit code
  end 
  else begin
    log('Fail');
    // handle failure if necessary; ResultCode contains the error code
  end;

end;
procedure Restore();
var 
  OutFile:  String;
  ResultCode: Integer;
begin
  OutFile:=ExpandConstant('{#restoreBatch}');
    //delete first
  DeleteFile(OutFile);

  //build batch file
  SaveStringToFile(OutFile,'REM *** Restore Application ***' + #13#10, True);
  SaveStringToFile(OutFile,'REM *** define variables ***' + #13#10, True);
  SaveStringToFile(OutFile,'SET gitPath={#gitPath}' + #13#10, True);
 
  SaveStringToFile(OutFile,'SET version=' + ExpandConstant('{#oldVersion}') + #13#10, True);
  SaveStringToFile(OutFile,'SET workingDrive=' + ExpandConstant('{#workingDrive}') + #13#10, True);
  SaveStringToFile(OutFile,'SET appDir=' + ExpandConstant('{app}') + #13#10, True);
  SaveStringToFile(OutFile,'SET backupTime=%date:~0,10% %time%' + #13#10, True);  
  SaveStringToFile(OutFile,'SET backupMsg="restore, %backupTime%"' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** setting the git.exe into system path ***' + #13#10, True);
  SaveStringToFile(OutFile,'PATH %gitPath%;%PATH%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** change the directory to AOM directory ***' + #13#10, True);
  SaveStringToFile(OutFile,'%workingDrive%' + #13#10, True);
  SaveStringToFile(OutFile,'cd %appDir%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** add and commit current version ***' + #13#10, True);
  SaveStringToFile(OutFile,'git add --all' + #13#10, True);
  SaveStringToFile(OutFile,'git commit -m %backupMsg%' + #13#10, True);
  
  SaveStringToFile(OutFile,'REM *** restore files ***' + #13#10, True);
  SaveStringToFile(OutFile,'git reset --hard %version%' + #13#10, True);
  
  //Execute batchFile
  if Exec(OutFile, '', '', SW_SHOW,
     ewWaitUntilTerminated, ResultCode) then
  begin
    log('Success');
    // handle success if necessary; ResultCode contains the exit code
  end 
  else begin
    log('Fail');
    // handle failure if necessary; ResultCode contains the error code
  end;
end;
